#!/bin/bash

# Quick Install: (  Zabbix 4.2, Mysql 5.6, Zabbix Agent 4.2 )
# By tsleite@bsd.com.br


pass-db=Lab2020

#rpm -Uvh https://repo.zabbix.com/zabbix/4.2/rhel/7/x86_64/zabbix-release-4.2-1.el7.noarch.rpmyum
rpm -ivh https://repo.zabbix.com/zabbix/4.2/rhel/7/x86_64/zabbix-release-4.2-2.el7.noarch.rpm

clean all
yum install zabbix-server-mysql zabbix-web-mysql zabbix-agent yum-utils -y
rpm -ivh http://repo.mysql.com/mysql-community-release-el7-5.noarch.rpm
yum install mysql-server -y
systemctl status mysqld


# For Zabbix
mysql -uroot -p -e 'create database zabbix character set utf8 collate utf8_bin;'
mysql -uroot -p -e "grant all privileges on zabbix.* to zabbix@localhost identified by '$pass-db';"
mysql -uroot -p -e "flush privileges;"

# Schema Zabbix Mysql
#zcat /usr/share/doc/zabbix-server-mysql*/create.sql.gz | mysql -uzabbix -p zabbix

# mysql_secure_installation
# rpm -e mysql-community-release-el7-5.noarch
# use mysql;
# update user set password=PASSWORD("GIVE-NEW-ROOT-PASSWORD") where User='root';
# flush privileges;

# Confs
grep "DBPassword=" /etc/zabbix/zabbix_server.conf
if [ $? -eq 0 ]; then sed -i "s/^.*DBPassword=.*$/DBPassword=$(pass-db)/g" /etc/zabbix/zabbix_server.conf fi

sed -i "s/^.*php_value date.timezone .*$/php_value date.timezone Europe\/Riga/" /etc/httpd/conf.d/zabbix.conf
grep -v "^$\|^#" /etc/zabbix/zabbix_server.conf ; echo

grep "^Alias" /etc/httpd/conf.d/zabbix.conf
if [ $? -ne 0 ]; then echo Alias not found in "/etc/httpd/conf.d/zabbix.conf". Something is out of order fi


#replace one line:
#Alias /zabbix /usr/share/zabbix-agent
#with two lines
#<VirtualHost *:80>
#DocumentRoot /usr/share/zabbix
sed -i "s/Alias \/zabbix \/usr\/share\/zabbix/<VirtualHost \*:80>\nDocumentRoot \/usr\/share\/zabbix/" /etc/httpd/conf.d/zabbix.conf

#add to the end of the file:
#</VirtualHost>
grep "</VirtualHost>" /etc/httpd/conf.d/zabbix.conf
if [ $? -eq 0 ]; then
echo "</VirtualHost>" already exists in the file /etc/httpd/conf.d/zabbix.conf
else
echo "</VirtualHost>" >> /etc/httpd/conf.d/zabbix.conf
fi
sed -i "s/^/#/g" /etc/httpd/conf.d/welcome.conf



# Firewall and Selinux
yum install policycoreutils-python -y

getsebool -a | grep "httpd_can_network_connect \|zabbix_can_network"
setsebool -P httpd_can_network_connect on
setsebool -P zabbix_can_network on
getsebool -a | grep "httpd_can_network_connect \|zabbix_can_network"

curl https://support.zabbix.com/secure/attachment/53320/zabbix_server_add.te > zabbix_server_add.te
checkmodule -M -m -o zabbix_server_add.mod zabbix_server_add.te
semodule_package -m zabbix_server_add.mod -o zabbix_server_add.pp
semodule -i zabbix_server_add.pp

#setenforce 0 && gentenforce
firewall-cmd --permanent --zone=public --add-rich-rule='rule family="ipv4" source address="$(hostname -I)/32" port protocol="tcp" port="10050" accept'
firewall-cmd --reload

systemctl start mysqld httpd zabbix-agent
systemctl enable zabbix-server mysqld httpd zabbix-agent


systemctl start zabbix-server
if [ $? -ne 0 ]; then
grep "denied.*zabbix.*server" /var/log/audit/audit.log | audit2allow -M zabbix_server
semodule -i zabbix_server.pp
fi

echo -e " Url: http://$(hostname -I)/zabbix/ \nUser: Admin \nPassword: zabbix"